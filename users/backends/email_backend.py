from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

User = get_user_model()

# The authenticate method attempts to authenticate a user based on email and password
class EmailAuthBackend(ModelBackend):
    """
    Custom authentication backend to login users using email address.
    """
    # Try to retrieve a user object by email. Here, 'username' parameter is used to pass the email.
    def authenticate(self, request, username=None, password=None):
        try:
            user = User.objects.get(email=username)
            if user.check_password(password):
                return user
            return
        except User.DoesNotExist:
            return

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
