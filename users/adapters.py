from allauth.account.adapter import DefaultAccountAdapter
from decouple import config
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

# custom activate_url to redirect to frontend
class CustomAccountAdapter(DefaultAccountAdapter):
    # override this method to change the email confirmation url
    def get_email_confirmation_url(self, request, emailconfirmation):
        # generate the frontend url with the emailconfirmation key
        custom_url = f"http://{config('FRONTEND_DOMAIN')}/{config('FRONTEND_ACTIVATE_CONFIRM_EMAIL_URL')}/{emailconfirmation.key}/"
        return custom_url
    
    # override send_mail method to send email to the user
    def send_mail(self, template_prefix, email, context):
        msg = EmailMultiAlternatives(
            subject=context.get('email_subject', 'Activate your account!'),
            body=render_to_string('account/email/email_confirmation_message.txt', context),
            from_email='no-reply@example.com',
            to=[email]
        )
        
        # read the html template
        html_email_template_name = render_to_string('account/email/email_confirmation_message.html', context)
        if html_email_template_name:
            msg.attach_alternative(
                html_email_template_name, 'text/html'
            )
        msg.send()

