from django.urls import include, path
from rest_framework.routers import DefaultRouter

from orders.views import OrderItemViewSet, OrderViewSet

app_name = "orders"

router = DefaultRouter()
# "^(?P<order_id>\d+)/order-items" is a regular expression that captures the order_id from the URL
# e.g. /orders/1/order-items/ will capture the order_id 1
router.register(r"^(?P<order_id>\d+)/order-items", OrderItemViewSet)
router.register(r"", OrderViewSet)


urlpatterns = [
    path("", include(router.urls)),
]
