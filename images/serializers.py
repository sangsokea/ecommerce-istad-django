from rest_framework import serializers
from .models import ProductImage, ProfileImage, IconImage, BrandImage, CategoryImage

class ProductImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImage
        fields = "__all__"

class ProfileImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProfileImage
        fields = "__all__"

class IconImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = IconImage
        fields = "__all__"

class BrandImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = BrandImage
        fields = "__all__"

class CategoryImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoryImage
        fields = "__all__"
