from django.contrib import admin

# Register your models here.
from .models import ProfileImage, ProductImage, IconImage, BrandImage, CategoryImage

admin.site.register(ProfileImage)
admin.site.register(ProductImage)
admin.site.register(IconImage)
admin.site.register(BrandImage)
admin.site.register(CategoryImage)

