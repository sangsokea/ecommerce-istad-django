from django.db import models

# Create your models here.

# share common fields and behavior.
class BaseImage(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        # abstract = True: means that this model will not be created in the database
        abstract = True


class ProductImage(BaseImage):
    image = models.ImageField(upload_to="product_images/")

    def __str__(self):
        return self.name
    
class CategoryImage(BaseImage):
    image = models.ImageField(upload_to="category_images/")

    def __str__(self):
        return self.name
    
class BrandImage(BaseImage):
    image = models.ImageField(upload_to="brand_images/")

    def __str__(self):
        return self.name
    
class ProfileImage(BaseImage):
    image = models.ImageField(upload_to="profile_images/")

    def __str__(self):
        return self.name
    
class IconImage(BaseImage):
    image = models.ImageField(upload_to="icon_images/")

    def __str__(self):
        return self.name