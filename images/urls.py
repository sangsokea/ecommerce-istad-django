from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ProfileImageViewSet, ProductImageViewSet, IconImageViewSet, BrandImageViewSet, CategoryImageViewSet

app_name = "images"

router = DefaultRouter()
router.register("profile", ProfileImageViewSet)
router.register("product", ProductImageViewSet)
router.register("icon", IconImageViewSet)
router.register("brand", BrandImageViewSet)
router.register("category", CategoryImageViewSet)


urlpatterns = [
    path("", include(router.urls)),
]