from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from .models import ProfileImage, ProductImage, IconImage, BrandImage, CategoryImage
from .serializers import ProfileImageSerializer, ProductImageSerializer, IconImageSerializer, BrandImageSerializer, CategoryImageSerializer

class ProfileImageViewSet(viewsets.ModelViewSet):
    queryset = ProfileImage.objects.all()
    serializer_class = ProfileImageSerializer

class ProductImageViewSet(viewsets.ModelViewSet):
    queryset = ProductImage.objects.all()
    serializer_class = ProductImageSerializer

class IconImageViewSet(viewsets.ModelViewSet):
    queryset = IconImage.objects.all()
    serializer_class = IconImageSerializer

class BrandImageViewSet(viewsets.ModelViewSet):
    queryset = BrandImage.objects.all()
    serializer_class = BrandImageSerializer

class CategoryImageViewSet(viewsets.ModelViewSet):
    queryset = CategoryImage.objects.all()
    serializer_class = CategoryImageSerializer