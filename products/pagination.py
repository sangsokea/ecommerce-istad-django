from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10 # Define how many records to show per page
    page_size_query_param = "page_size" # Allow the client to override the page size
    max_page_size = 100 # Maximum page size the client may request

    def get_paginated_response(self, data):
        return Response(
            {
                "next": self.get_next_link(),
                "previous": self.get_previous_link(),
                "total": self.page.paginator.count,
                "results": data,
            }
        )