#!/bin/sh

echo 'Waiting for postgres...'

# nc is a command-line tool used to read and write data across network connections using the TCP/IP protocol.
# -z option is used to tell nc to report open ports, rather than initiate a connection.

# The while loop will keep running until the nc command returns a successful exit status, which means that the PostgreSQL server is up and running.
while ! nc -z $DB_HOSTNAME $DB_PORT; do
    sleep 0.1
done

echo 'PostgreSQL started'

echo 'Running migrations...'
python manage.py makemigrations
python manage.py migrate

echo 'Collecting static files...'
# collectstatic is a Django management command that handles the collection of static files from your apps into a single location that can easily be served in production.
python manage.py collectstatic --no-input

exec "$@"